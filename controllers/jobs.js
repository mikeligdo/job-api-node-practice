const Job = require('../models/Job')
const CustomAPIError = require('../errors/custom-error')

const getAllJobs = async (req, res) => {
    const jobs = await Job.find({createdBy: req.user.userId}).sort('createdAt')
    res.status(200).json(jobs)
}
const getJob = async (req, res) => {
    const { user:{ userId }, params:{ id:jobId } } = req
    const job = await Job.findOne({  _id: jobId, createdBy: userId })
    if (!job) {
        throw new CustomAPIError('No Job Found', 404)
    }
    res.status(200).json(job)
}
const createJob = async (req, res) => {
    req.body.createdBy = req.user.userId
    const job = await Job.create(req.body)
    res.status(200).json({job})
}
const updateJob = async (req, res) => {
    const { body:{ company, position}, user:{ userId }, params:{ id:jobId } } = req

    if (company === '' || position === '') {
        throw new CustomAPIError('Please Fill All the data', 404)
    }

    const job = await Job.findOneAndUpdate({ _id:jobId, createdBy: userId }, req.body, {new: true, runValidators: true})

    if (!job) {
        throw new CustomAPIError('No Job Found', 404)
    }

    res.status(200).json(job)

}
const deleteJob = async (req, res) => {
    const { user:{ userId }, params:{ id:jobId } } = req

    const job = await Job.findByIdAndRemove({_id: jobId, createdBy: userId})

    if (!job) {
        throw new CustomAPIError('No Job Found', 404)
    }

    res.status(200).send('Job Deleted')
}


module.exports = { getAllJobs, getJob, createJob, updateJob, deleteJob}